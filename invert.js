function invert(testObject){
    if (Object.keys(testObject).length === 0 || typeof testObject !== 'object') {
        return "Object is empty or it is not type of object"
    }
    else{
        const invertArray={}
        for(let key in testObject){
            invertArray[testObject[key]]=key
        }
        return invertArray
    }
}

module.exports=invert