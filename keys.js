
function keys(testObject){
    if (Object.keys(testObject).length === 0 || typeof testObject !== 'object') {
        return "Object is empty or it is not type of object"
    }
    else{
        const arrayKeys=[]
        for(let key in testObject){
            arrayKeys.push(key)
        }
        return arrayKeys
    }
}

module.exports=keys