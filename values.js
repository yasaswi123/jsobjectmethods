function values(testObject){
    if (Object.keys(testObject).length === 0 || typeof testObject !== 'object') {
        return "Object is empty or it is not type of object"
    }
    else{
        let valueArray=[]
        for(let key in testObject){
            valueArray.push(testObject[key])
        }
        return valueArray
    }
}

module.exports = values