function mapObject(testObject,cb){
    if (Object.keys(testObject).length === 0 || typeof testObject !== 'object') {
        return "Object is empty or it is not type of object"
    }
    else{
        const mapObjects={}
        for(let key in testObject){
            mapObjects[key]=cb(key,testObject)
        }
        return mapObjects;
    }
}

module.exports=mapObject