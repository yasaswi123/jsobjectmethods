const defaultProps={name:'aa',age:24,location:'andhra'}

function defaults(testObject,defaultProps){
    if (Object.keys(testObject).length === 0 || typeof testObject !== 'object') {
        return "Object is empty or it is not type of object"
    }
    else{
        for(let key in testObject){
            if(testObject[key]==undefined){
                testObject[key]=defaultProps[key]
            }
            return testObject
        }
    }
}

module.exports={defaults,defaultProps}