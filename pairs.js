function pairs(testObject){
    if (Object.keys(testObject).length === 0 || typeof testObject !== 'object') {
        return "Object is empty or it is not type of object"
    }
    else{
        const pairsArray=[]
        for(let key in testObject){
            pairsArray.push([key,testObject[key]])
        }
        return pairsArray
    }
}

module.exports=pairs